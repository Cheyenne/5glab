%./ulsim -m8 -s10 -e1 -w1 -a -r20 -f 1 -i sGFDM.txt -j SIR_dB
%SIR_dB, TP (% of max)
TP_GFDM = [
100, 100;
80, 100;
60, 100;
40, 100;
20, 100;
10, 99.9;
0, 99.9;
-3, 98.5;
-5, 92.3;
-6, 84.9;
-7, 76.1;
-8, 51.3;
-9, 36.2;
-10, 32.8;
-11, 18.2;
-12, 2.4;
-13, 0];

%./ulsim -m8 -s10 -e1 -w1 -a -r20 -f 1 -i sOFDM.txt -j SIR_dB
TP_OFDM = [
100, 100;
80, 100;
60, 100;
40, 99.9;
20, 100;
10, 99.9;
5, 99.2;
3, 90.0;
2, 88.5;
1, 77.8;
0, 55.6;
-1, 50.4;
-2, 38.1;
-3, 33.8;
-4, 32.7;
-5, 29.9;
-6, 17.2;
-7, 3.2;
-8, 0];

%./ulsim -m8 -s10 -e1 -w1 -a -r20 -f 1 -i sSCFDMA.txt  -j SIR_dB
TP_SCFDMA = [
0, 100;
-5, 99.9;
-8, 99.7;
-10, 99.3;
-11, 98.7;
-12, 97.5;
-13, 96.1;
-14, 93.7;
-15, 83.1;
-16, 69.1;
-17, 58.3;
-18, 36.2;
-19, 31.3;
-20, 1.5;
-21, 0];

%%
set(0, 'DefaultLineMarkerSize', 10);
set(0, 'Defaultaxesfontsize', 14);
set(0, 'DefaultLineLineWidth', 2.5);

h=figure(3);
TP_max = 837600/1024;
hold off
plot(-TP_GFDM(:,1),TP_GFDM(:,2)/100*TP_max,'b')
hold on
plot(-TP_OFDM(:,1),TP_OFDM(:,2)/100*TP_max,'k--')
plot(-TP_SCFDMA(:,1),TP_SCFDMA(:,2)/100*TP_max,'r-.')
legend('GFDM','OFDM','SC-FDMA','Location','SouthWest')
xlabel('ISR [dB]')
ylabel('L2 Goodput [kbps]')
xlim([-20 20])
ylim([0 1000])
grid on
saveas(h,'simulation_results.eps','epsc2')