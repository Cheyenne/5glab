function test_suite = test_get_ambgfun()
    initTestSuite;

function test_correctValue
    p = get_defaultGFDM('BER');

    am = get_ambgfun(p);

    assertEqual(size(am), [p.M, p.K]);
    assertElementsAlmostEqual(am(1,1), 1);
    assertElementsAlmostEqual(am(3,4), 0.000482002779);
