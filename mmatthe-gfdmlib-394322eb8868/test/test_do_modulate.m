function test_suite = test_do_modulate
    initTestSuite;

function test_small
    p = get_defaultGFDM('small');
    p.L = p.K;
    
    D = do_map(p, qammod(get_random_symbols(p), 2^p.mu, 0, 'gray'));

    x1 = do_modulate(p, D);
    x2 = get_transmitter_matrix(p) * D(:);

    assertElementsAlmostEqual(x1, x2);
