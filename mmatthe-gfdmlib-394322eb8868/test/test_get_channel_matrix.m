function [ test_suite ] = test_get_channel_matrix(  )
    initTestSuite;
 
function test_correct_matrix
    h = [1 2 3];
    H = get_channel_matrix(h, 4);
    Hexp = [1 0 3 2;
            2 1 0 3;
            3 2 1 0;
            0 3 2 1];
    assertElementsAlmostEqual(Hexp, H);
