function test_suite = test_get_receiver_matrix()
    initTestSuite;
    
function test_matched_filter
    p = get_defaultGFDM('small');
    A = get_transmitter_matrix(p);
    
    assertElementsAlmostEqual(A', get_receiver_matrix(p, 'MF'));
    
function test_zero_forcing
    p = get_defaultGFDM('small');
    A = get_transmitter_matrix(p);
    
    assertElementsAlmostEqual(get_receiver_matrix(p, 'ZF')*A, eye(size(A)));
        