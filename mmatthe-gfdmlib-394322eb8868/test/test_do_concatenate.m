function test_suite = test_do_concatenate
    initTestSuite;

function test_concatenate
    p = get_defaultGFDM('small');
    p.overlap_blocks = 1;
    B = 4;
    N = p.K*p.M + p.Ncp + p.Ncs;
    
    x = ones(N ,B);
    
    tx = do_concatenate(p,x);

    assertEqual(reshape(repmat([2;ones(N-2*p.overlap_blocks,1)],B,1),[],1), tx);
    assertEqual(length(tx), B*(N-p.overlap_blocks));

