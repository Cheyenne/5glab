function test_suite = test_do_split
    initTestSuite;

function test_split
    p = get_defaultGFDM('small');
    p.overlap_blocks = 1;
    B = 4;
    N = p.K*p.M+p.Ncp+p.Ncs;
    
    tx = repmat([2;ones(N-2*p.overlap_blocks,1)], B, 1);
    
    rx = do_split(p,tx);

    assertEqual(repmat([2;ones(N-2*p.overlap_blocks,1);2], 1, B), rx);
    
function test_splitRevertsConcatenate
    p = get_defaultGFDM('small');
    B = 4;
    
    N = p.K*p.M+p.Ncp+p.Ncs;
    
    d = randn(N, B);
    tx = do_concatenate(p, d);
    dh = do_split(p, tx);
    
    assertEqual(d, dh);
