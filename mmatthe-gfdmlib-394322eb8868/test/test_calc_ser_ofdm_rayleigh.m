function test_suite = test_calc_ser_ofdm_rayleigh
    initTestSuite;

function test_flatChannel_values
    assertElementsAlmostEqual(0.1384059288, calc_ser_ofdm_rayleigh(2, 7, 1));

    assertElementsAlmostEqual([0.1932196594, 0.1384059288], calc_ser_ofdm_rayleigh(2, [5, 7], 1));