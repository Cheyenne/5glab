function test_suite = test_calc_ser_awgn
    initTestSuite;

function test_flatChannel_ZF
    p = get_defaultGFDM('BER');

    assertElementsAlmostEqual(0.00145102, calc_ser_awgn(p, 'ZF', 11));
    assertElementsAlmostEqual([0.02403486, 0.00145102], calc_ser_awgn(p, 'ZF', [8, 11]));


function test_FSC_ZF
    p = get_defaultGFDM('BER');

    assertElementsAlmostEqual(0.01803381, calc_ser_awgn(p, 'ZF', 11, [1 0 0 0 0.5]));

function test_flatChannel_MF
    p = get_defaultGFDM('BER');

    assertElementsAlmostEqual(0.02580763, calc_ser_awgn(p, 'MF', 11));

function test_FSC_MF
    p = get_defaultGFDM('BER');

    assertElementsAlmostEqual(0.03871012, calc_ser_awgn(p, 'MF', 11, [1 0 0 0 0.5]));