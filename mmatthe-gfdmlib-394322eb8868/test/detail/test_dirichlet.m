function test_suite = test_dirichlet
    initTestSuite;

function test_oddM_isReal
    g = dirichlet(5, 16);
    assertElementsAlmostEqual(sum(abs(imag(g))), 0);
    assertElementsAlmostEqual(sum(abs(g).^2), 1);

function test_evenM_isImag
    g = dirichlet(6, 16);
    assertTrue(sum(abs(imag(g))) > 0);
    assertElementsAlmostEqual(sum(imag(g)), 0);
    assertElementsAlmostEqual(sum(abs(g).^2), 1);
