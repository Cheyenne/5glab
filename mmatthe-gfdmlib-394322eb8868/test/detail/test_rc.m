function test_suite = test_rc
    initTestSuite;

function test_hasZeros
    g = rc(5, 16, 0.5);
    assertElementsAlmostEqual(g(17:16:end), [0 0 0 0]');

function test_isNormalized
    g = rc(5, 16, 0.25);
    assertElementsAlmostEqual(sum(abs(g).^2), 1);