function test_suite = test_rect_fd()
    initTestSuite;
    
function test_correctSpectrum()
    M = 8; K = 128;
    g = get_pulse(M);
    G = fft(g) / sqrt(M*K);
    
    assertElementsAlmostEqual(sum(abs(g.*g)), 1);
    assertElementsAlmostEqual(sum(abs(G.*G)), 1);
    assertElementsAlmostEqual(G(1:M/2), ones(M/2, 1)/sqrt(M));
    assertElementsAlmostEqual(G(end-M/2+1:end), ones(M/2, 1)/sqrt(M));
    
function g=get_pulse(M)
p = get_defaultGFDM('BER');
p.M = M;
p.pulse = 'rect_fd';
g = get_transmitter_pulse(p);
