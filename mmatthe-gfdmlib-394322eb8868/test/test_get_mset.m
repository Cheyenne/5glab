function test_suite = test_get_mset
    initTestSuite;

function test_fullSet
    p = struct;
    p.M = 5;

    assertEqual(0:4, get_mset(p));

function test_Mon
    p = struct;
    p.M = 5;
    p.Mon = 4;

    assertEqual(1:4, get_mset(p));

function test_Mset
    p = struct;
    p.M = 5;
    p.Mset = [0, 2, 4];

    assertEqual([0, 2, 4], get_mset(p));

function test_MonToHigh_Error
    p = struct;
    p.M = 5; p.Mon = 6;

    assertExceptionThrown(@() get_mset(p), 'MATLAB:assertion:failed');
    
   
function test_WrapsAround
    p = struct;
    p.M = 5;
    p.Mset = -1:1;
    
    assertEqual([4,0,1], get_mset(p));

function test_ErrorOnDoubleAllocation
    p = struct; p.M = 5;
    p.Mset = [0, 1, 2, 1];
    
    assertExceptionThrown(@() get_mset(p), 'MATLAB:assertion:failed');
