function test_suite = test_get_kset
initTestSuite;

function test_fullSet
p = struct;
p.K = 6;

assertEqual(0:5, get_kset(p));

function test_Kon
p = struct;
p.K = 6;
p.Kon = 5;

assertEqual([0 1 2 4 5], get_kset(p));

function test_Kset
p = struct;
p.K = 6;
p.Kset = [2 3 5 7];
p.Kon = 5;

assertEqual(mod(p.Kset, p.K), get_kset(p));


function test_KonTooHigh_error
p = struct;
p.K = 16;
p.Kon = 20;

assertExceptionThrown(@() get_kset(p), 'MATLAB:assertion:failed');


function test_KsetWrapsAround
p = struct;
p.K = 16;
p.Kset = -3:3;

assertEqual([13, 14, 15, 0, 1, 2, 3], get_kset(p));

function test_doubleAllocationIsError
p = struct;
p.K = 16;
p.Kset = [0, 1, 2, 3, 0];

assertExceptionThrown(@() get_kset(p), 'MATLAB:assertion:failed');
