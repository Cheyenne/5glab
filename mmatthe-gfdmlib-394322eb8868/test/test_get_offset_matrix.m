function test_suite = test_get_offset_matrix
    initTestSuite;
end

function test_0dF1dT_shiftsOneSymbol
    p = get_defaultGFDM('small');
    Q = get_offset_matrix(p, 0, 1);
    
    assertElementsAlmostEqual(Q, circshift(eye(p.K*p.M), [p.K, 0]));
end

function test_1dF0dT_shiftsOneSubcarrier
    p = get_defaultGFDM('small');
    Q = get_offset_matrix(p, 1, 0);

    F = dftmtx(p.M*p.K) / sqrt(p.M*p.K);
    assertElementsAlmostEqual(F*Q*F', circshift(eye(p.K*p.M), [p.M, 0]));
end