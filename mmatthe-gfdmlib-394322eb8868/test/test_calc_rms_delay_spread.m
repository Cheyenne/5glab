function test_suite = test_calc_rms_delay_spread
    initTestSuite;


function test_simple
    P = [1];
    tau = [0];

    assertEqual(0, calc_rms_delay_spread(P, tau));

function test_twoTaps
    P = [1 1];
    tau = [0 1];
    assertEqual(0.5, calc_rms_delay_spread(P, tau));