\documentclass{scrartcl}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage[framemethod=tikz]{mdframed}
\usepackage{verbatim}

\title{Workflow and Coding Style}
\author{Maximilian Matthe}

\newcommand{\code}[1]{\texttt{#1}}
\newenvironment{remark}{\vspace{2mm}\begin{mdframed}[hidealllines=true,backgroundcolor=gray!20]\paragraph{Remark:}}{\end{mdframed}}
\newenvironment{git}{\begin{alltt}}{\end{alltt}}

\newcommand{\gitcmd}[2]{\vspace*{2mm}\newline\vspace*{2mm}%
  \begin{tabular}{|p{8cm}|p{8cm}}%
    \textbf{Command line} & \textbf{git gui}\\%
    \begin{minipage}{8cm}\begin{alltt}#1\end{alltt}\end{minipage} & %
    \begin{minipage}{8cm}\begin{alltt}#2\end{alltt}\end{minipage}%
  \end{tabular}}%


\begin{document}
\maketitle

This document describes the Git version control system and the
corresponding workflow for users and collaborators of the GFDM Matlab
library.  Furthermore, remarks on the proposed coding style are given.

\section{Git}
Git is a distributed version control system.  There are plenty of
tutorials on the internet and these will not be recreated here.
Instead, the links to these tutorials are given in the following.

Actually, Git can be easily controlled from the command line,
but there are also GUI programs for Git, the most prominent may be
TortoiseGit.  People that are used to the command line are encouraged
to used git in the command line interface of git for several reasons:

\begin{itemize}
\item You will know better what you are doing
\item It's easier to write specific tutorials for the command line
\item You will be able to use git on any PC that has Git installed (no
  dependency on any extra program)
\end{itemize}

However, the simple \code{git gui} will be shortly described also.

\subsection{Git in the Command line}
To install Git for the command line, I suggest to install the msysgit
package.  It is available at
\url{file:////ifn/mns/proj/5GNOW/code/msysgit.zip}. Just unpack it
somewhere on your hard drive. You can then enter the git command line
by executing the file \code{git-cmd.bat}.

\begin{remark}
  A more sophisticated command line environment for Windows is the
  Cygwin package that emulates a Unix command line with all the Unix
  command line tools like \code{find, grep, less} and so on.  For
  sure, it also contains Git. Leave me a note if you are interested.
\end{remark}

\subsection{Git with a GUI}
The msysgit package also contains a small GUI for git, named
\code{gitk} to view a graphical log of your working copy.  Furthermore
the command \code{git gui} exists that can be used to actually modify
your repository.  Start both programs by entering \code{gitk} and
\code{git gui} into the git command line.  Commands and workflow
presented in this tutorial will be using this GUI.

\subsection{Git Tutorials}
The internet is full of tutorials on git.  A really good one is
ProGit, which can be found here: \url{http://git-scm.com/book/en/}.

For a short overview I recommend reading the following:

\begin{itemize}
\item
  \url{http://git-scm.com/book/en/Getting-Started-About-Version-Control}
  A good overview of differences between centralized (SVN)
  and distributes VCS (e.g. Git).
\item \url{http://git-scm.com/book/en/Getting-Started-Git-Basics}
  gives a rough overview of the git workflow.
\item
  \url{http://git-scm.com/book/en/Git-Basics-Getting-a-Git-Repository}
  How to clone a repository from the internet.
\item
  \url{http://git-scm.com/book/en/Git-Basics-Recording-Changes-to-the-Repository}
  How to add, change and commit files.
\item \url{http://git-scm.com/book/en/Git-Basics-Working-with-Remotes}
  How to work with remotes - A remote is a repository that is not your
  local repository.
\end{itemize}

Some more advanced topics include the following links which are not
directly required by from start are treated here:
\begin{itemize}
\item \url{http://git-scm.com/book/en/Getting-Started-Getting-Help}
  How to get help for specific commands
\item
  \url{http://git-scm.com/book/en/Git-Basics-Viewing-the-Commit-History}
  How to see log messages and changes
\item \url{http://git-scm.com/book/en/Git-Branching} How to work with
  branches, especially:
  \begin{itemize}
  \item \url{http://git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging}
  \end{itemize}
\end{itemize}


\section{Workflow}
This chapter describes the workflow for users and collaborators.

The master branch at the bitbucket server will always contain a stable
version of the library that can be checked out by users that do only
want to use the library.  Collaborators are asked to create an own
branch and commit changes to this branch.  When the feature is ready,
Max will merge it into the master branch, so that other users and
collaborators can benefit from it.

\subsection{As a user}
\begin{enumerate}
\item Register at Bitbucket.org: You need to have an account at
  bitbucket.org; use your \code{ifn.et.tu-dresden.de} mail address
  here. After having registered, tell Max about your user name to get
  access to the library.
\item Clone the repository:
  \begin{git}
    git clone https://<yourname>@bitbucket.org/mmatthe/gfdmlib.git
  \end{git}
  This step cannot be carried out with the GUI.

\item Remove any changes you have made to the code:
  \gitcmd{git reset -{}-hard}{Branch->Reset...->Yes}

  This resets your working copy to the state it had been directly
  after checking out the current version.

\item Get the current version from the internet
\gitcmd{git pull origin master}{Remote->Fetch
  from->origin\\Merge->Local merge...->\\Tracking
  branch->origin/master->Merge}

This only works, if your working copy is clean, i.e. you have no
modifications in your working copy.
\end{enumerate}

\subsection{As a collaborator}
As a collaborator you shall commit to a feature branch with your
name since you do not have write-access to the master branch on
bitbucket.  Here is the corresponding workflow:

\begin{enumerate}
\item Create your branch
\gitcmd{git checkout -b yourname}{Branch->Create...\\Insert
  Name\\Create}
This creates a branch named \code{yourname} and sets the working copy
to commit to this branch.

\item Check that you are on your feature branch
\gitcmd{git branch\\The output shall show a * next to your branch}{See
  the text field\\Current Branch: <yourname>}

\item Add a modified file to the index (optional)\\
\gitcmd{git add filename}{Mark the file to be added and
  choose\\Commit->Stage to Commit}
This command tells git which files shall be saved when the commit
action is issued. Files that are not added to the index will not be
committed.

\item Commit files that are in the index
\gitcmd{git commit -m ``Your Commit Message goes here''}{1. Enter the
  commit message\\2. Commit->Commit}

\item Commit all files that have been changed since last checkout
  \gitcmd{git commit -a -m ``Your commit message goes
    here''}{1. Commit->Stage changed files to commit\\2. Enter commit
    message\\3. Commit->Commit}
\item Update your feature branch with the newest contents of the
  master branch.\\
  This only works, when your working copy is clean, i.e. you dont have
  any modified files.
\gitcmd{git fetch\\git merge master}{Remote->Fetch
  from...->origin\\Merge->Local merge...->Tracking
  branch->origin/master\\Merge}

\item Push your new features to your internet branch
  \gitcmd{git push origin yourbranchname}{Remote->Push...\\Source
    branch->yourbranchname\\Push}
  Now you need to notify Max about the changes and he will merge them
  into the master branch.
\end{enumerate}


\section{Coding Style}
Function and variable names are written
\code{with\_underscore\_between\_words}. Constants shall be written in
capital letters.  When the GFDM parameter object \code{p} is required
by a function, it shall be passed as the first argument.

\subsection{Unit tests}
Every function shall be accompanied by a unit test function.  The unit
testing facility in Matlab is implemented by the 3rd party library
\code{xunit} which is part of the repository.  Unit tests are small
functions that test particular aspects of the functions that are
proivided in the library.  A script runs all these tests and checks,
wether all tests are still passing.  If they dont, something is going
wrong.

By writing unit tests, the following advantages are achieved:

\begin{itemize}
\item The probability of accidentically modifying the behavior of
  source code is highly reduced.
\item The unit test can inherently be used as examples on
  how to use the functions.
\item By providing unit tests the library design becomes more
  uncoupled since each function should be testable more or less
  standalone.
\item When even writing the test before writing the implementation you
  need to think before you write, how your new function or
  functionality should be used.  This increases code quality.
\end{itemize}

The following code shows an example of a unit test file.  It checks,
wether the mapping mechanism of the GFDM library works as expected.

\begin{verbatim}
function test_suite = test_do_map  % Common header for each unit test
    initTestSuite;

function test_fullMap              % Test, if full mapping works correct
    p = get_defaultGFDM('small');  % arrange
    p.M = 2; p.K = 3; p.Kon = 3;

    s = 1:6;                       % action
    D = do_map(p, s);

    assertEqual([1 4; 2 5; 3 6], D); % assert

function test_kset              % Test, if it works when not all
                                % subcarriers are allocated
    p = get_defaultGFDM('small');
    p.M = 2;
    p.K = 3; p.Kset = [0 2];

    s = [1:4]';
    D = do_map(p, s);

    assertEqual([1 3; 0 0; 2 4], D);

function test_mset  % test, if it works when not all subsymbols are allocated
     p = get_defaultGFDM('small');
     p.M = 3; p.Mset = [0 2];
     p.K = 2; p.Kon = 2;

     s = [1:4]';
     D = do_map(p, s);

     assertEqual([1 0 3; 2 0 4], D);
\end{verbatim}

The test precisely describes, what should happen, when a function is
called.  In this case, a linear data stream should be mapped to the
GFDM transmission matrix, but not all subsymbols and subcarriers are
always allocated.

\end{document}
